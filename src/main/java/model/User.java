package model;

import controller.HeroControllerInterface;

public class User {
    private String name;
    private HeroControllerInterface hero;

    public User(String name) {
        this.name = name;
    }

    public void setHero(HeroControllerInterface hero) {
        this.hero = hero;
    }

    public String getName() {
        return name;
    }

    public HeroControllerInterface getHero() {
        return hero;
    }

    @Override
    public String toString() {
        return name;
    }
}

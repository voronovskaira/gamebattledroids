package model;

import constant.Const;

public class HeroModel {
    private String name;
    private int healthPoint;
    private int attack;
    private int armor;
    private double criticalHitChance;
    private double criticalHit;
    private double avoidChance;
    private boolean isBlock;

    public HeroModel(String name, int healthPoint, int attack, int armor, double criticalHitChance, double criticalHit, double avoidChance, boolean isBlock) {
        this.name = name;
        this.healthPoint = healthPoint;
        this.attack = attack;
        this.armor = armor;
        this.criticalHitChance = criticalHitChance;
        this.criticalHit = criticalHit;
        this.avoidChance = avoidChance;
        this.isBlock = isBlock;
    }

    public void healHero(int hp) {
        this.healthPoint += hp;
    }

    public boolean isAlive() {
        return healthPoint > 0;
    }

    @Override
    public String toString() {
        return this.name + Const.HP + this.healthPoint;
    }

    public void setHealthPoint(int healthPoint) {
        this.healthPoint -= healthPoint;
    }

    public String getName() {
        return name;
    }

    public int getAttack() {
        return attack;
    }

    public int getArmor() {
        return armor;
    }

    public double getCriticalHitChance() {
        return criticalHitChance;
    }

    public double getCriticalHit() {
        return criticalHit;
    }

    public double getAvoidChance() {
        return avoidChance;
    }

    public boolean isBlock() {
        return isBlock;
    }

    public void setBlock(boolean block) {
        isBlock = block;
    }
}

import controller.Arena;
import view.ConsoleView;

public class MainMenu {

    private static void runGame() {
        Arena arena = new Arena(new ConsoleView());
        arena.starFightOneVsOne();
    }

    public static void main(String[] args) {
        runGame();
    }
}

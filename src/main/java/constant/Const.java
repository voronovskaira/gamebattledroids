package constant;

public class Const {
    public static String HERO_MENU = "   Choose action :\n1 - attack the enemy\n2 - put a block\n3 - skill attack";
    public static String INCORRECT_VALUE = "Incorrect! Enter value from 1 to ";
    public static String DAMAGE_MESSAGE = " did the damage ";
    public static String AVOID_DAMAGE_MESSAGE = " avoid damage";
    public static String BLOCK_DAMAGE_MESSAGE = " block damage";
    public static String HP = " \nHEALTH POINT: ";
    public static String WIN_MESSAGE = " won. Congratulations!!!";
    public static String INVALID_VALUE = "Incorrect value. Try again";
    public static String CHOOSE_HERO = "Choose your hero\n";
    public static String CHOOSE_ENEMY = "Choose your enemy\n";
    public static String NICKNAME = "Enter your nickname";
    public static String PATH_TO_CONFIG_PROPERTIES = "src/main/resources/config.properties";

    private Const() {
    }
}

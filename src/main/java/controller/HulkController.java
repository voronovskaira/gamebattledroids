package controller;

import model.HeroModel;
import view.View;

public class HulkController extends HeroController {

    HulkController(HeroModel hero, View view) {
        super(hero, view);
    }

    @Override
    public void superAttack(HeroControllerInterface target) {
        super.superAttack(target);
        target.receiveDamage(getDamage() * 2);
    }
}

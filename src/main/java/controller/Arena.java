package controller;

import constant.Const;
import model.User;
import utils.ConsoleReader;
import view.View;

public class Arena {
    private View view;
    private HeroFabric heroFabric;
    private ConsoleReader consoleReader;

    public Arena(View view) {
        this.view = view;
        this.consoleReader = new ConsoleReader(view);
        this.heroFabric = new HeroFabric(view);
    }

    public void starFightOneVsOne() {
        User firstUser = createUser(consoleReader.scanFromConsole(Const.NICKNAME));
        chooseHero(firstUser, Const.CHOOSE_HERO);
        User secondUser = createUser("Enemy");
        chooseHero(secondUser, Const.CHOOSE_ENEMY);
        showWinner(startFight(firstUser, secondUser));
    }

    private User createUser(String userName) {
        User user = new User(userName);
        return user;
    }

    private void chooseHero(User user, String message) {
        int value = consoleReader.readValue(message + heroFabric.getFabricDescription(), heroFabric.getFabricSize());
        HeroControllerInterface hero = heroFabric.createHero(value);
        user.setHero(hero);
        view.print(user.getName() + " chose " + hero.getHeroDescription());
    }

    private void controlHero(User user, User enemy) {
        int value = consoleReader.readValue(Const.HERO_MENU, 3);
        switch (value) {
            case 1:
                user.getHero().attack(enemy.getHero());
                break;
            case 2:
                user.getHero().useBlock();
                break;
            case 3:
                user.getHero().superAttack(enemy.getHero());
                break;
            default:
                view.print(Const.INVALID_VALUE);
                controlHero(user, enemy);
        }
        view.print(enemy.getHero().getHeroDescription());
    }

    private User startFight(User user, User enemy) {
        while (user.getHero().isAlive() && enemy.getHero().isAlive()) {
            controlHero(user, enemy);
            controlHero(enemy, user);
        }
        return user.getHero().isAlive() ? user : enemy;
    }

    private void showWinner(User user) {
        view.print(user + Const.WIN_MESSAGE);
    }
}

package controller;

import constant.Const;
import model.HeroModel;
import utils.RandomNumberGenerator;
import view.View;

public abstract class HeroController implements HeroControllerInterface {

    HeroModel hero;
    View view;

    HeroController(HeroModel hero, View view) {
        this.hero = hero;
        this.view = view;
    }

    public int attack(HeroControllerInterface target) {
        int damage = target.receiveDamage(getDamage());
        view.print(hero.getName() + Const.DAMAGE_MESSAGE + damage);
        return damage;
    }

    public void superAttack(HeroControllerInterface target) {
        view.print(hero.getName() + " use super attack");
    }

    public void useBlock() {
        hero.setBlock(true);
    }

    public int receiveDamage(double damage) {
        int receivedDamage = 0;
        if (RandomNumberGenerator.isLuckyChance(hero.getAvoidChance())) {
            view.print(hero.getName() + Const.AVOID_DAMAGE_MESSAGE);
        } else if (hero.isBlock()) {
            view.print(hero.getName() + Const.BLOCK_DAMAGE_MESSAGE);
            receivedDamage = (int) damage / 2 - hero.getArmor();
            hero.setHealthPoint(receivedDamage);
            hero.setBlock(false);
        } else {
            receivedDamage = (int) damage - hero.getArmor();
            hero.setHealthPoint(receivedDamage);
        }
        return receivedDamage;
    }

    public String getHeroDescription() {
        return hero.toString();
    }

    int getDamage() {
        return RandomNumberGenerator.isLuckyChance(hero.getCriticalHitChance())
                ? (int) (hero.getAttack() * hero.getCriticalHit()) : hero.getAttack();

    }

    public boolean isAlive() {
        return hero.isAlive();
    }
}

package controller;

import model.HeroModel;
import view.View;

public class VampireController extends HeroController {

    VampireController(HeroModel hero, View view) {
        super(hero, view);
    }

    @Override
    public void superAttack(HeroControllerInterface target) {
        super.superAttack(target);
        int damageDone = super.attack(target);
        view.print(hero.getName() + " healed for " + damageDone + " health points");
        hero.healHero(damageDone);
    }
}

package controller;

import model.HeroModel;
import view.View;

public class AssassinController extends HeroController {

    public AssassinController(HeroModel hero, View view) {
        super(hero, view);
    }

    @Override
    public void superAttack(HeroControllerInterface target) {
        super.superAttack(target);
        for (int i = 0; i < 3; i++)
            super.attack(target);
    }
}

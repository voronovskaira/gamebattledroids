package controller;

public interface HeroControllerInterface {

    int attack(HeroControllerInterface target);

    void superAttack(HeroControllerInterface target);

    void useBlock();

    boolean isAlive();

    int receiveDamage(double damage);

    String getHeroDescription();

}

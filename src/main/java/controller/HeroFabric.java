package controller;

import model.HeroModel;
import utils.PropertiesReader;
import view.View;

import java.util.HashMap;
import java.util.Map;

class HeroFabric {

    private Map<Integer, HeroControllerInterface> heroes;
    private View view;

    HeroFabric(View view) {
        this.view = view;
        heroes = createHeroes();
    }

    HeroControllerInterface createHero(int value) {
        return heroes.get(value);
    }

    int getFabricSize() {
        return heroes.size();
    }

    String getFabricDescription() {
        StringBuilder description = new StringBuilder();
        for (Map.Entry<Integer, HeroControllerInterface> entry : heroes.entrySet()) {
            description.append(entry.getKey().toString()).append(" - ").append(entry.getValue().getHeroDescription()).append("\n");
        }
        return description.toString();
    }

    private Map<Integer, HeroControllerInterface> createHeroes() {
        Map<Integer, HeroControllerInterface> fabric = new HashMap<>();
        fabric.put(1, new VampireController(new HeroModel(PropertiesReader.readProperties("vampire_name"),
                Integer.parseInt(PropertiesReader.readProperties("vampire_hp")),
                Integer.parseInt(PropertiesReader.readProperties("vampire_attack")),
                Integer.parseInt(PropertiesReader.readProperties("vampire_armor")),
                Double.parseDouble(PropertiesReader.readProperties("vampire_critical_hit_chance")),
                Integer.parseInt(PropertiesReader.readProperties("vampire_critical_hit")),
                Double.parseDouble(PropertiesReader.readProperties("vampire_avoid_chance")),
                Boolean.parseBoolean(PropertiesReader.readProperties("vampire_is_block"))), view));
        fabric.put(2, new AssassinController(new HeroModel(PropertiesReader.readProperties("assassin_name"),
                Integer.parseInt(PropertiesReader.readProperties("assassin_hp")),
                Integer.parseInt(PropertiesReader.readProperties("assassin_attack")),
                Integer.parseInt(PropertiesReader.readProperties("assassin_armor")),
                Double.parseDouble(PropertiesReader.readProperties("assassin_critical_hit_chance")),
                Integer.parseInt(PropertiesReader.readProperties("assassin_critical_hit")),
                Double.parseDouble(PropertiesReader.readProperties("assassin_avoid_chance")),
                Boolean.parseBoolean(PropertiesReader.readProperties("assassin_is_block"))), view));
        fabric.put(3, new HulkController(new HeroModel(PropertiesReader.readProperties("hulk_name"),
                Integer.parseInt(PropertiesReader.readProperties("hulk_hp")),
                Integer.parseInt(PropertiesReader.readProperties("hulk_attack")),
                Integer.parseInt(PropertiesReader.readProperties("hulk_armor")),
                Double.parseDouble(PropertiesReader.readProperties("hulk_critical_hit_chance")),
                Integer.parseInt(PropertiesReader.readProperties("hulk_critical_hit")),
                Double.parseDouble(PropertiesReader.readProperties("hulk_avoid_chance")),
                Boolean.parseBoolean(PropertiesReader.readProperties("hulk_is_block"))), view));
        return fabric;
    }
}

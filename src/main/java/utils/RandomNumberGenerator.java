package utils;

import java.util.Random;

public class RandomNumberGenerator {

    public static boolean isLuckyChance(double chance) {
        Random random = new Random();
        double randomNumber = random.nextDouble();
        return randomNumber <= chance;
    }

}

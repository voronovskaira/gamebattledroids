package utils;

import constant.Const;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class PropertiesReader {
    private static String value;

    public static String readProperties(String key) {
        Properties property = new Properties();
        try {
            FileInputStream file = new FileInputStream(Const.PATH_TO_CONFIG_PROPERTIES);
            property.load(file);
            value = property.getProperty(key);
        } catch (IOException e) {
            System.err.println("Error");
        }
        return value;
    }
}
